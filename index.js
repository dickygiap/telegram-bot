const express = require('express')
const app = new express()
const teleBot = require('./botHelpers')

app.get('/', (req, res) => {
  teleBot.sendMessage(process.env.CHAT_ID, 'Test Tele', {
    replyMarkup: teleBot.inlineKeyboard([
      [
        teleBot.inlineButton('approve', { callback: 'approve?tid=1' }),
        teleBot.inlineButton('reject', { callback: 'reject?tid=1' })
      ]
    ])
  })
  return res.sendStatus(200)
})

const port = process.env.PORT || 3001
app.listen(port, (req, res) => {
  console.log(`running on port ${port}`)
})