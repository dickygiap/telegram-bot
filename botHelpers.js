const TeleBot = require('telebot')
const teleBot = new TeleBot({ token: process.env.TELEBOT_TOKEN })
teleBot.on('callbackQuery', (msg) => {
  console.log('msg', msg)
  try {
    if (/approve\?tid=[A-Za-z0-9]*/.test(msg.data)) {
      console.log('Approve')
    } else {
      console.log('Reject')
    }
    teleBot.sendMessage(msg.message.chat.id, `${msg.data} is approved by @${msg.from.username}`)
    teleBot.editMessageReplyMarkup({ chatId: msg.message.chat.id, messageId: msg.message.message_id }, { replyMarkup: teleBot.inlineKeyboard([]) })
    return teleBot.answerCallbackQuery(msg.id, { text: `Approve: ${msg.data}`, showAlert: true })
  } catch (error) {
    console.error(error)
  }

})
teleBot.start();
module.exports = teleBot